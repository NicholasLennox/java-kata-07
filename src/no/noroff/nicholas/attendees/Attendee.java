package no.noroff.nicholas.attendees;

import no.noroff.nicholas.competebehaviours.CompeteBehaviour;
import no.noroff.nicholas.competebehaviours.NoCompete;

import java.util.ArrayList;

public abstract class Attendee {
    protected String name;
    protected CompeteBehaviour competeBehaviour;

    public Attendee() {
    }

    public Attendee(String name) {
        this.name = name;
    }

    public Attendee(String name, CompeteBehaviour competeBehaviour) {
        this.name = name;
        this.competeBehaviour = competeBehaviour;
    }

    // Render method for children
    public abstract String render();
    // Competing in event, passing results list to behaviour
    public void Compete(ArrayList<String> results){
        this.competeBehaviour.compete(this.name, results);
    }

    // Getters and setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CompeteBehaviour getCompeteBehaviour() {
        return competeBehaviour;
    }

    public void setCompeteBehaviour(CompeteBehaviour competeBehaviour) {
        this.competeBehaviour = competeBehaviour;
    }
}
