package no.noroff.nicholas.attendees;

import no.noroff.nicholas.competebehaviours.CompeteBehaviour;
import no.noroff.nicholas.competebehaviours.NoCompete;

public class Marshal extends Attendee {
    public Marshal() {
        competeBehaviour = new NoCompete();
    }

    public Marshal(String name) {
        super(name);
        competeBehaviour = new NoCompete();
    }

    @Override
    public String render() {
        return "Hi, I am a marshal and my name is " + this.name;
    }
}
