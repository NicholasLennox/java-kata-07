package no.noroff.nicholas.attendees;

import no.noroff.nicholas.competebehaviours.CompeteBehaviour;

public class Athlete extends Attendee {
    public Athlete() {
    }

    public Athlete(String name, CompeteBehaviour competeBehaviour) {
        super(name, competeBehaviour);
    }
    @Override
    public String render() {
        return "Hi, I am a athlete and my name is " + this.name;
    }
}
