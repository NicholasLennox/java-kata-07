package no.noroff.nicholas.attendees;

import no.noroff.nicholas.competebehaviours.CompeteBehaviour;
import no.noroff.nicholas.competebehaviours.NoCompete;

public class Spectator extends Attendee {
    public Spectator() {
        competeBehaviour = new NoCompete();
    }

    public Spectator(String name) {
        super(name);
        competeBehaviour = new NoCompete();
    }

    @Override
    public String render() {
        return "Hi I am a spectator and my name is " + this.name;
    }
}
