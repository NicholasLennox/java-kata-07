package no.noroff.nicholas.competebehaviours;

import java.util.ArrayList;

public class NoCompete implements CompeteBehaviour {
    @Override
    public void compete(String name, ArrayList<String> results) {
        // Dont do anything
    }
}
