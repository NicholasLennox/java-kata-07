package no.noroff.nicholas.competebehaviours;

import java.util.ArrayList;

public class CompeteInRun implements CompeteBehaviour {
    @Override
    public void compete(String name, ArrayList<String> results) {
        results.add(name + ": is competing in the run");
    }
}
