package no.noroff.nicholas.competebehaviours;

import java.util.ArrayList;

public interface CompeteBehaviour {
    void compete(String name, ArrayList<String> results);
}
