package no.noroff.nicholas.heroStrat;

public class HeroAttribute {
    public int health;
    public int strength;
    public int dexterity;
    public int intelligence;
}
