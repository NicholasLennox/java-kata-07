package no.noroff.nicholas.heroStrat;

public class Hero {
    HeroType type;
    HeroAttribute baseStats;
    HeroAttributeStrategy attributeStrategy;

    public Hero(HeroType type, HeroAttributeStrategy attributeStrategy) {
        this.type = type;
        this.attributeStrategy = attributeStrategy;
        getBase();
    }

    private void getBase(){
        attributeStrategy.getBase(baseStats);
    }

    public void levelUp(){
        attributeStrategy.levelUp(baseStats);
    }
}
