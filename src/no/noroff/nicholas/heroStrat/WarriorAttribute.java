package no.noroff.nicholas.heroStrat;

public class WarriorAttribute implements HeroAttributeStrategy {
    @Override
    public void getBase(HeroAttribute stats) {
        stats.health = 150;
        stats.strength = 10;
        stats.dexterity = 5;
        stats.intelligence = 1;
    }

    @Override
    public void levelUp(HeroAttribute stats) {
        stats.health += 30;
        stats.strength += 5;
        stats.dexterity += 2;
        stats.intelligence += 1;
    }
}
