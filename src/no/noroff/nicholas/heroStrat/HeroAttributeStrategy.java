package no.noroff.nicholas.heroStrat;

public interface HeroAttributeStrategy {
    void getBase(HeroAttribute stats);
    void levelUp(HeroAttribute stats);
}
