package no.noroff.nicholas.heroes;

public class Mage extends Hero {


    public Mage() {
        health = 120;
        strength = 2;
        dexterity = 5;
        intelligence = 10;
    }

    @Override
    public void levelUp() {
        health += 30;
        strength += 5;
        dexterity += 2;
        intelligence += 1;
    }
}
