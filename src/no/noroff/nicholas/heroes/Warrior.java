package no.noroff.nicholas.heroes;

public class Warrior extends Hero {

    public Warrior() {
        health = 150;
        strength = 10;
        dexterity = 5;
        intelligence = 1;
    }

    @Override
    public void levelUp() {
        health += 30;
        strength += 5;
        dexterity += 2;
        intelligence += 1;
    }
}
