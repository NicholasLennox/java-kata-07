package no.noroff.nicholas.heroes;

public class Ranger extends Hero  {


    public Ranger() {
        health = 130;
        strength = 5;
        dexterity = 10;
        intelligence = 3;
    }

    @Override
    public void levelUp() {
        health += 30;
        strength += 5;
        dexterity += 2;
        intelligence += 1;
    }
}
