package no.noroff.nicholas.heroes;

public abstract class Hero {
    protected int health;
    protected int strength;
    protected int dexterity;
    protected int intelligence;

    public abstract void levelUp();
}
