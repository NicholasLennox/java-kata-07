package no.noroff.nicholas;

import no.noroff.nicholas.attendees.Marshal;
import no.noroff.nicholas.attendees.Spectator;
import no.noroff.nicholas.attendees.Athlete;
import no.noroff.nicholas.competebehaviours.CompeteInRun;
import no.noroff.nicholas.competebehaviours.CompeteInSki;
import no.noroff.nicholas.competebehaviours.CompeteInSwim;
import no.noroff.nicholas.competebehaviours.CompeteInTri;
import no.noroff.nicholas.event.Event;

import no.noroff.nicholas.heroStrat.Hero;
import no.noroff.nicholas.heroStrat.HeroType;
import no.noroff.nicholas.heroStrat.WarriorAttribute;
import no.noroff.nicholas.heroes.Warrior;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        // Create event
        Event myEvent = new Event("My First Event");
        // Add some attendees
        Marshal mrMarshal = new Marshal("John");
        Spectator genericSpectator = new Spectator("Jack");
        Athlete fullTri = new Athlete("Jeff", new CompeteInTri());
        Athlete runner = new Athlete("Geoff", new CompeteInRun());
        Athlete swimmer = new Athlete("Nick", new CompeteInSwim());
        Athlete skier = new Athlete("Amalie", new CompeteInSki());
        myEvent.addAttendee(mrMarshal);
        myEvent.addAttendee(genericSpectator);
        myEvent.addAttendee(fullTri);
        myEvent.addAttendee(runner);
        myEvent.addAttendee(swimmer);
        myEvent.addAttendee(skier);
        // Render
        ArrayList<String> renders = myEvent.renderAttendees();
        // Do event
        ArrayList<String> results =myEvent.completeEvent();

        // Print it all
        for (String render: renders) {
            System.out.println(render);
        }
        for (String result: results) {
            System.out.println(result);
        }

        Hero matt = new Hero(HeroType.WARRIOR, new WarriorAttribute());
    }
}
