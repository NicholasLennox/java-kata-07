package no.noroff.nicholas.event;

import no.noroff.nicholas.attendees.Attendee;

import java.util.ArrayList;

public class Event {
    private String name;
    private ArrayList<Attendee> attendees = new ArrayList<>();

    public Event() {
    }

    public Event(String name) {
        this.name = name;
    }

    public Event(String name, ArrayList<Attendee> attendees) {
        this.name = name;
        this.attendees = attendees;
    }

    // Render all the attendees at the event
    public ArrayList<String> renderAttendees(){
        ArrayList<String> renders = new ArrayList<>();
        for (Attendee at: attendees) {
            renders.add(at.render());
        }
        return renders;
    }

    // Do the event by competing
    public ArrayList<String> completeEvent(){
        ArrayList<String> results = new ArrayList<>();
        for (Attendee at: attendees) {
            at.Compete(results);
        }
        return results;
    }

    // Add attendees
    public void addAttendee(Attendee attendee){
        attendees.add(attendee);
    }

    // Getters and setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Attendee> getAttendees() {
        return attendees;
    }

    public void setAttendees(ArrayList<Attendee> attendees) {
        this.attendees = attendees;
    }
}
